<?php

//配置
define('APP_PATH', realpath(__DIR__ . '/../'));

$config_file = APP_PATH . '/app/config/config.ini';
if (file_exists(APP_PATH . '/app/config/config.dev.ini')) {
    $config_file = APP_PATH . '/app/config/config.dev.ini';
}
//引入配置文件
$config = new Phalcon\Config\Adapter\Ini($config_file);
//载入公共方法
include APP_PATH . $config->common->pluginsDir . "Common.php";

/* services */
// 实例化注入
$di = new Phalcon\DI\FactoryDefault();

//路由注入
$di->set('router', function () {
    return include APP_PATH . '/app/config/routes.php';
});

/* 数据库连接 */
$di->set('db', function () use ($config) {
    $connection = new \Phalcon\Db\Adapter\Pdo\Mysql(array(
        "host" => $config->db->host,
        "username" => $config->db->username,
        "password" => $config->db->password,
        "dbname" => $config->db->dbname,
        'charset' => $config->db->charset
    ));

    // 记录SQL日志  调度控制器
    if ($config->development->sql_record) {
        $eventsManager = new Phalcon\Events\Manager();
        //创建日志
        $logger = new Phalcon\Logger\Adapter\File($config->base->log_dir);
        //将一个侦听程序连接到事件管理器
        $eventsManager->attach('db', function ($event, $connection) use ($logger) {
            if ($event->getType() == 'beforeQuery') { //判断事件状态
            	//写入日志
                $logger->log($connection->getSQLStatement(), \Phalcon\Logger::INFO);
            }
        });
        //将侦听事件绑定到数据库连接中
        $connection->setEventsManager($eventsManager);
    }
    return $connection;
});

//开启session 
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

try {

	//创建应用
	$application = new Phalcon\Mvc\Application($di);

	//分别引入前后台配置
	$application->registerModules(
	    array(
	    	'frontend' => array(
	            'className' => 'Multiple\Frontend\Module',
	            'path'      => APP_PATH.'/app/frontend/Module.php',
	        ),
	        'backend'  => array(
	            'className' => 'Multiple\Backend\Module',
	            'path'      => APP_PATH.'/app/backend/Module.php',
	        )
	    )
	);
	//处理请求
	echo $application->handle()->getContent();

} catch(\Exception $e){
    echo $e->getMessage();
}
