/*
Navicat MySQL Data Transfer

Source Server         : 本地连接
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : xx_blog

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2015-12-27 16:32:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for access_log
-- ----------------------------
DROP TABLE IF EXISTS `access_log`;
CREATE TABLE `access_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ip` varchar(50) DEFAULT NULL COMMENT 'ip地址',
  `num` int(10) DEFAULT NULL COMMENT '访问次数',
  `sign` varchar(50) DEFAULT NULL COMMENT '标记',
  `last_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `log_time` datetime DEFAULT NULL COMMENT '首次登录时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of access_log
-- ----------------------------

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `headline` varchar(255) DEFAULT NULL,
  `content` text,
  `tags` varchar(255) DEFAULT NULL COMMENT '标签组',
  `pid` int(10) DEFAULT '0' COMMENT '所属群组',
  `layout` enum('0','2','1') DEFAULT '0' COMMENT '0不同布局 1 首页显示2 级显示',
  `sort` int(3) DEFAULT '0' COMMENT '排序 根据权重 计算',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('1', 'php7', 'php7 测试', '1,2,3,4,5,6', '1', '1', null, null, null);

-- ----------------------------
-- Table structure for criticism
-- ----------------------------
DROP TABLE IF EXISTS `criticism`;
CREATE TABLE `criticism` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT '评论人',
  `content` varchar(255) DEFAULT NULL,
  `pid` int(10) DEFAULT NULL COMMENT '对应的文章id',
  `log_time` datetime DEFAULT NULL COMMENT '日志时间',
  `ip` varchar(50) DEFAULT NULL COMMENT '评论人 ip',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of criticism
-- ----------------------------

-- ----------------------------
-- Table structure for image
-- ----------------------------
DROP TABLE IF EXISTS `image`;
CREATE TABLE `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL COMMENT '图片地址',
  `name` varchar(50) DEFAULT NULL COMMENT '图片名称',
  `status` enum('1','0') DEFAULT '1' COMMENT '1显示 0 隐藏',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of image
-- ----------------------------

-- ----------------------------
-- Table structure for parameters
-- ----------------------------
DROP TABLE IF EXISTS `parameters`;
CREATE TABLE `parameters` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '名字',
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1 显示 0 隐藏',
  `url` varchar(255) DEFAULT NULL COMMENT '路径',
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  `update_time` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of parameters
-- ----------------------------
INSERT INTO `parameters` VALUES ('1', '备案号', '1', null, '2015 &copy; www.china李.ren&nbsp;&nbsp;&nbsp;&nbsp;   京ICP备15065734号-1', null, null);
INSERT INTO `parameters` VALUES ('2', 'Email', '1', null, 'lbf2131@163.com  ', null, null);
INSERT INTO `parameters` VALUES ('3', 'Phone', '1', null, '18879396086', null, null);

-- ----------------------------
-- Table structure for tags
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT '标签名',
  `status` enum('0','1') DEFAULT '1' COMMENT '1显示,0 隐藏',
  `sort` int(3) DEFAULT NULL COMMENT '排序 根据权重排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tags
-- ----------------------------

-- ----------------------------
-- Table structure for theme
-- ----------------------------
DROP TABLE IF EXISTS `theme`;
CREATE TABLE `theme` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '主题名字',
  `status` enum('0','1') DEFAULT '1' COMMENT '1显示 0 隐藏',
  `content` varchar(255) DEFAULT NULL,
  `pid` int(10) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL COMMENT '路径',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of theme
-- ----------------------------
INSERT INTO `theme` VALUES ('1', 'PHP', '1', '', '0', null);
INSERT INTO `theme` VALUES ('2', 'Linux', '1', null, '0', null);
INSERT INTO `theme` VALUES ('3', 'Mysql', '1', null, '0', null);
INSERT INTO `theme` VALUES ('4', 'Phalcon', '1', null, '1', null);
INSERT INTO `theme` VALUES ('5', 'Thinkphp', '1', null, '1', null);
INSERT INTO `theme` VALUES ('6', 'Centos', '1', null, '2', null);
INSERT INTO `theme` VALUES ('7', 'Ubuntu', '1', null, '2', null);
INSERT INTO `theme` VALUES ('8', 'Sql', '1', null, '3', null);
INSERT INTO `theme` VALUES ('9', 'NoSql', '1', null, '0', null);
INSERT INTO `theme` VALUES ('10', '前端', '1', null, '0', null);
INSERT INTO `theme` VALUES ('11', 'HTML', '1', null, '10', null);
INSERT INTO `theme` VALUES ('12', 'CSS', '1', null, '10', null);
INSERT INTO `theme` VALUES ('13', 'JavaScript', '1', null, '10', null);
INSERT INTO `theme` VALUES ('14', 'AngularJS', '1', null, '10', null);
INSERT INTO `theme` VALUES ('15', 'jQuery', '1', null, '10', null);
INSERT INTO `theme` VALUES ('16', 'Pages', '1', '页数', '0', null);
INSERT INTO `theme` VALUES ('17', 'Photo', '1', '照片', '16', null);
INSERT INTO `theme` VALUES ('18', 'Schedule', '1', '时间表', '16', null);
INSERT INTO `theme` VALUES ('19', 'Undergo', '1', '经历', '16', null);
INSERT INTO `theme` VALUES ('20', 'Planning', '1', '规划', '16', null);
