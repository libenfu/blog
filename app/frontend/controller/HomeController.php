<?php
/**
 * Created by PhpStorm.
 * User: lbf
 * Date: 2015/12/21
 * Time: 23:38
 */
namespace Multiple\Frontend\Controllers;
use Multiple\Frontend\Models;
class HomeController extends ControllerBase
{
	public function initialize()
	{
        parent::initialize(); //调用父类方法
        global $config;
    	//创建登录日志
    	$ip = $_SERVER['REMOTE_ADDR'];//访客ip
    	$log = new Models\Access();
    	//findFirst 查询是 输出一维数组 find 多个查询 输出二维数组 toArray() 更改输出格式
    	$visit = $log->findFirst("ip='$ip'");//->toArray();  ->dump();
        $mtime = explode(' ',microtime()); //微秒 级
        $startTime = $mtime[1]+$mtime[0];
    	if($visit){ 
            //统计方法放在公共控制器中 当用户访问一个页面 实际公共控制器调用了多次 设置时间限制
            if((($startTime-strtotime($visit->last_time))) > 3600) //访问后一小时内不计算访问量
              $log->logChange($visit->id);//更新日志
        }else{
            $log->logCreate($ip);//创建日志
        }
	}
    // 首页显示
    public function indexAction()
    {

    }
    //入口页面
    public function generalAction()
    {

    }


}
