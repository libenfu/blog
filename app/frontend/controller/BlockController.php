<?php
namespace Multiple\Frontend\Controllers;

use Multiple\Frontend\Models;
class BlockController extends ControllerBase
{
    public $tagsModel;

    public function initialize()
    {
        $this->tagsModel = new Models\Tags(); //标签
        $this->themeModel = new Models\Theme();//主题
    }

    public function indexAction()
    {

    }
    //头部logo 菜单
    public function headerAction()
    {
        

        $list = $this->themeModel->find(array("status = '1'"))->toArray();
        //两层目录
        $data = [];
        foreach ($list as $k => $v) {
            if($v['pid'] == 0)
                $data[$v['id']] = $v;
        }
        foreach ($list as $key => $value) {
           foreach ($data as $k => $v) {
                if($k == $value['pid'])
                    $data[$k]['up'][$value['id']] = $value;
            }
        }
        $this->view->data = $data;        


    }

    public function footerAction()
    {
        //页脚控制
    }

    public function sidebarAction()
    {
        //菜单资源
    }

    public function page_headAction()
    {
        // 页眉 控制
    }

    public function quickAction()
    {
        //快捷栏
    }
    //标签
    public function tagsAction()
    {
        
    }
    
    //右侧列表 根据状态显示 sort 倒序
    public function right_listAction()
    {
        $list = $this->tagsModel->find(array("status = '1'","order"=>"sort desc"));
        $this->view->tags_list = $list;
    }
    

}
