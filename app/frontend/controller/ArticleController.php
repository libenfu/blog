<?php
/**
 * Created by PhpStorm.
 * User: lbf
 * Date: 2015/12/28
 * Time: 23:38
 */
namespace Multiple\Frontend\Controllers;

use Multiple\Frontend\Models;
class ArticleController extends ControllerBase
{
    public $articleModel;
    public $imageModel;

    public function initialize()
    {
        $this->articleModel = new Models\Article();
        $this->imageModel = new Models\Image();
        $this->tagsModel = new Models\Tags();
    }
    // 文章展示页面
    public function indexAction()
    {
    	$id = $this->request->get('id');
        $list = $this->articleModel->findFirst(array("id='$id'",'order'=>'create_time desc'));
        $img  = $this->imageModel->findFirst("pid='{$id}'");
        $tags = $this->tagsModel->find(array("id in({$list->tags})",'order'=>'sort desc','limit'=>'5'));
        $list->img = $img; //图片
        $list->tags = $tags; //标签
        $list->create_time = substr($list->create_time,0,10); //创建时间
        $this->view->list = $list;
    }

}
