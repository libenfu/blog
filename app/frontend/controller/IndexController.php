<?php
/**
 * Created by PhpStorm.
 * User: lbf
 * Date: 2015/12/21
 * Time: 23:38
 */
namespace Multiple\Frontend\Controllers;

class IndexController extends ControllerBase
{

    // 默认入口
    public function indexAction()
    {
    	header('location: /home');
    	exit;
    }

}
