<?php
/**
 * Created by PhpStorm.
 * User: lbf
 * Date: 2015/12/21
 * Time: 23:38
 */
namespace Multiple\Frontend\Controllers;
use Phalcon\Mvc\Controller;
//公共参数
class ControllerBase extends Controller
{
	//初始加载
    public function initialize()
    {
    	global $config;
    	//设置时区
    	ini_set("date.timezone", $config['base']['time_zone']);
    	//前端 不考虑 权限问题但是 记录访问ip
    	//域名
    	define('HTTP_HOST',$_SERVER['HTTP_HOST']);
    	$this->view->Host = 'http://'.HTTP_HOST; //分配地址模版变量
    }
}
