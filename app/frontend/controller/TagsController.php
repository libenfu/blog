<?php
/**
 * Created by PhpStorm.
 * User: lbf
 * Date: 2016/1/1
 * Time: 2:03
 */
namespace Multiple\Frontend\Controllers;

use Multiple\Frontend\Models;
class TagsController extends ControllerBase
{
    public  $articleModel;
    public function initialize()
    {
        $this->articleModel = new Models\Article();
    }

    public function indexAction()
    {
        $id = $this->request->get('id');
        $list = $this->articleModel->find(array("tags like '%$id%'",'order'=>'create_time desc'));
        $this->view->list = $list;

    }

}