<?php
//配置
namespace Multiple\Frontend;
use Phalcon\Loader,
    Phalcon\Mvc\Dispatcher,
    Phalcon\Mvc\View,
    Phalcon\DiInterface,
    Phalcon\Mvc\ModuleDefinitionInterface,
    Phalcon\Mvc\View\Engine\Volt;
class Module implements ModuleDefinitionInterface
{
    /**
     * 注册一个特定模块的自动装卸机
     * 1.30 升级到 2.0 后registerAutoloaders 和 registerServices 需要设置默认值
     */
    public function registerAutoloaders(DiInterface $di = null)
    {
        $loader = new Loader();
        //将后台控制器和模型文件加载如命名中间
        $loader->registerNamespaces(
            array(
                'Multiple\Frontend\Controllers' => __DIR__.'/controller/',
                'Multiple\Frontend\Models'      => __DIR__.'/model/',
            )
        );

        $loader->register();
    }

    /**
     * 注册模块的特定服务
     */
    public function registerServices(DiInterface $di = null)
    {
        //组装一个调度程序
        $di->set('dispatcher', function() {
            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace("Multiple\Frontend\Controllers");
            return $dispatcher;
        });

        //注册视图组件
        $di->set('view', function() {
            $view = new View();
            $view->setViewsDir('../app/frontend/view/');
            $view->registerEngines(array(
                '.phtml' => function ($view, $di){
                    //分配模版变量
                    $volt = new Volt($view, $di);
                   /* 创建缓存 */
                    $volt->setOptions(array(
                        'compiledPath' => '../app/cache/',
                        'compiledSeparator' => '_'
                    ));
                    return $volt;
                }
            ));
            return $view;
        },true);
    }

}
