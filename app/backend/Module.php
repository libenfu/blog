<?php
//配置
namespace Multiple\Backend;
use Phalcon\Loader,
	Phalcon\Mvc\Dispatcher,
	Phalcon\Mvc\View,
    Phalcon\DiInterface,
	Phalcon\Mvc\ModuleDefinitionInterface;

class Module implements ModuleDefinitionInterface
{
    /**
     * 注册一个特定模块的自动装卸机
     */
    public function registerAutoloaders(DiInterface $di = null)
    {

        $loader = new Loader();
        //将后台控制器和模型文件加载如命名中间
        $loader->registerNamespaces(
            array(
                'Multiple\Backend\Controllers' => '../app/backend/controller/',
                'Multiple\Backend\Models'      => '../app/backend/model/',
            )
        );

        $loader->register();
    }

    /**
     * 注册模块的特定服务
     */
    public function registerServices(DiInterface $di = null)
    {

        //组装一个调度程序
        $di->set('dispatcher', function() {
            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace("Multiple\Backend\Controllers");
            return $dispatcher;
        });

        //注册视图组件
        $di->set('view', function() {
            $view = new View();

            $view->setViewsDir('../app/backend/view/');
            return $view;
        });
    }

}
