<?php
namespace Multiple\Backend\Controllers;

use Phalcon\Mvc\Controller;
class PublicController extends Controller
{

    // 操作提示
    public function tipAction()
    {
        $do = $this->request->get('do');

        $tip = $this->session->get('tip');
        $message = isset($tip['message']) ? $tip['message'] : '操作成功';
        $seconds = isset($tip['seconds']) ? $tip['seconds'] : 2;
        $error = isset($tip['error']) ? $tip['error'] : 0;
        $jump = isset($tip['jump']) ? $tip['jump'] : '/home';
        $this->view->tip = array(
            'error' => $error,
            'code' => 'ok',
            'message' => $message,
            'seconds' => $seconds,
            'jump' => 'http://' . $_SERVER['HTTP_HOST'] . $jump
        );
        if ($do == 'full') {
            $this->view->pick("public/tip_full");
        }
    }
    //退出
    public function logoutAction()
    {
        header('Location: http://www.example.com/');
        exit;
    }


}
