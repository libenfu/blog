<?php
/* 通过设置路由来做到分区*/
// 设置需要的路由
$router = new \Phalcon\Mvc\Router();
//设置默认模块
$router->setDefaultModule("frontend");
//$router->setDefaultNamespace('Multiple\Frontend\Controllers');//默认命名空间

//登录
$router->add("/login", array(
	'module' => 'backend', //模块
    'controller' => 'public',
    'action' => 'login',
));
//退出
$router->add("/logout", array(
	'module' => 'backend', 
    'controller' => 'public',
    'action' => 'logout',
));
//进入后台
$router->add("/admin/info/:action", array(
	'module' => 'backend', 
    'controller' => 'info',
    'action' => 1
));
//前台
$router->add("/:controller/:action", array(
    'controller' => 1,
    'action' => 2
));

$router->add("/home", array(
    'controller' => 'home',
    'action' => 'general'
));

//提示
$router->add("/tip", array(
    'controller' => 'public',
    'action' => 'tip',
));

// 所有路由都自定义notFound才可用
// $router = new \Phalcon\Mvc\Router(False);
// $router->notFound(array('controller'=>'public','action'=>'error'));

//处理结尾额外的斜杆
$router->removeExtraSlashes(true);

return $router;